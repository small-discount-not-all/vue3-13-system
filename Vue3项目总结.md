# Vue3 项目-13-总结

## 1.初始化项目

Vite项目的基础必须要有Node.js。Node官网：http://nodejs.cn/download/

搭建Vite项目：

> ```js
> // 安装命令:
> npm init @vitejs/app
> ```
>
> <img src="D:\9.笔记\06-javaScript笔记\JS现代教程\images\bbb51e602ef82234ca930b81c349f78f.webp" style="zoom:55%;" />

Vite项目的结构目录：

> ```js
> .
> ├── README.md
> ├── index.html           入口文件
> ├── package.json
> ├── public               资源文件
> │   └── favicon.ico
> ├── src                  源码
> │   ├── App.vue          单文件组件
> │   ├── assets
> │   │   └── logo.png
> │   ├── components   
> │   │   └── HelloWorld.vue
> │   └── main.js          入口
> └── vite.config.js vite工程化配置文件
> ```
>
> vite项目初始化时，询问了我们初始化什么项目，选择Vue之后，初始化完成后就有了Vue.js插件。

### 1.1vite的常用配置

参考连接：https://juejin.cn/book/6933939264455442444/section/6933954904104894479

公共基础地址:

> 打包后在/dist/index.html中体现，也就是公共基础地址是打包后的资源公共地址。
>
> ```js
> // vite.config.js公共部分
> import { defineConfig } from 'vite'
> import vue from '@vitejs/plugin-vue'
> 
> export default defineConfig({
>   plugins: [vue()],  
> })
> ```
>
> 
>
> base配置项：
>
> ```js
> export default defineConfig({
>   plugins: [vue()]  
>   base: './'  // 公共基础地址
> })
> ```
>
> resolve.alias配置路径别名
>
> ```js
> ...
> import path from 'path'
> 
> // https://vitejs.dev/config/
> export default defineConfig({
>  ......
>   resolve: {
>     // 别名设置
>     alias: {
>       '@': path.resolve(__dirname, '/src')
>     }
>   }
> })
> // Vite 1.0 是需要用 /@/ ，加斜杠的形式，Vite 2.0 后，便优化了。
> ```
>
> resolve.extensions：导入文件时，需要省略的扩展名列表
>
> ```js
> export default defineConfig({
>   plugins: [vue()]  
>   base: './',
>   resolve: {
>     // 别名设置
>     alias: {
>       '@': path.resolve(__dirname, '/src')
>     },
>     extensions: ['.mjs', '.js', '.ts', '.jsx', '.tsx', '.json','.vue'] // 默认值
>   }
> })
> ```
>
> server：该配置内置多种开发时常用的选项
>
> ```js
> // https://vitejs.dev/config/
> export default defineConfig({
>     ...
>   resolve: {
>       ...
>   },
>   server: {
>     // 指定服务器主机名
>     host: '0.0.0.0',
>     // 开发环境启动的端口号
>     port: 3008,
>     // 是否在开发环境下自动打开应用程序
>     open: true,
>     // 代理
>     proxy: {
>       '/api': {
>         target: 'http://jsonplaceholder.typicode.com',
>         changeOrigin: true,
>         rewrite: (path) => path.replace(/^\/api/, '')
>       }
>     }
>   }
> })
> ```

### 1.2环境变量的配置





## 2.项目所需其它插件

### 2.1导入vue-router插件

在根目录下，运行命令安装vue-router插件

```js
npm i vue-router@next
```

> @next的意思为最新版本的，安装vue-router用来构建单页应用。

安装完成后，在`src`目录下，新建`router/inde.js`文件，并添加路由配置项。添加完毕后，在mian.js文件中，引入router并使用(use(router))。





